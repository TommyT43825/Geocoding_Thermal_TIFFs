# Instructions for Using the Geocoding Thermal Tiffs Tool
To navigate to the Project Proposal/reason for the creation of the tool [link to project homepage](https://gitlab.com/TommyT43825/Geocoding_Thermal_TIFFs/wikis/home)

For detailed instructions on how to use the tool click the [link to detailed instruction](https://gitlab.com/TommyT43825/Geocoding_Thermal_TIFFs/wikis/instructions-for-using-geocoding-themral-tiffs-tool)

To look at psuedo code clike the [link to psuedo code](https://gitlab.com/TommyT43825/Geocoding_Thermal_TIFFs/wikis/psuedocode-for-my-script)

It is important to have the following python modules and tools installed on your computer (links to download tools can be found below).  

1. import logging
2. import gdal, glob, numpy, time, os, ogr
3. from datetime import datetime, timedelta
4. from Tkinter import *
5. import tkFileDialog, tkMessageBox
6. import exiftool


This tool will only work with thermal sensors with the resoulution of 640X512. The csv files need to meet the following two critera and the user is responsible in order to ensure that the input csv files are properly formatted:

1. The header needs to be in this order with tempreture values represented in each of the cells

2. The csv file needs to have 640 columns and 512 rows in order to properly create the tiff raster

## File Structure is Important
In order to use this script you will need to first make sure tha you have the required tools and file structure. The first thing you will need to have downloaded and installed is the exiftool and the pyexiftool. These tools and their documentation can be found at the following websites:

1. Be sure to extract the .exe file to the same folder as your script. "ExifTool is a platform-independent Perl library plus a command-line application for reading, writing and editing meta information in a wide variety of files." More information can be found by clicking the link below. 
Download tool from [here](http://www.sno.phy.queensu.ca/~phil/exiftool/)   

2. Be sure to extract the files to the same folder as your script (might need everything from this zip to be legal).  "a Python library to communicate with an instance of Phil Harvey's excellent ExifTool command-line application." More information can be found by clicking the link below.
Download tool from [here](https://github.com/smarnach/pyexiftool and extract the exiftool.py) 
 
3. Exif meta data documentation can be found [here.](http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/GPS.html)


## Input, Output, and Parameters of Interest. 
The Tkinter GUI created  has 5 different fields that the user will have populate.  The first field allows the user to navigate for the directory where the input gpx file is located.  The second field allows the user to enter in the input folder directory where the csv files reside.  The third field is the output directory where the geo-tagged images will be placed.  The  fourth field is the input year (this is needed to assign the year to the date metadata file for the tiff image. The fifth and final field is to allow the user to enter the number of hours they need to adjust by in order to put time stamps into local time zones.

## Graphical User Interface
1. Open GUI:
2. Enter in the gpx (this will only take gpx files)
3. Enter the input folder
4. Enter the output folder
5. If a folder already conatains tiff files this message window will ask you if you would like to overwrite these files.
6. Enter in the year that matches the csv time stamps with the gpx time stamps
7. Be sure to put in correct time zones.
