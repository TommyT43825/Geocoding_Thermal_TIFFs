#  Date 12/05/2016
#  Author: Tommy Thompson
#  Ecology Graduate Student @ Utah Stat University
#  This GUI is for using a themal csv data sets (with the resolution of 640X512)
#  as input to create a tiff  and geotag each tiff image using gps data
#  the geotaged image time stamps must match timestamps from gps data
#****************************************************************************************************************

# import required modules and tools
import gdal, glob, numpy, time, os, ogr
import exiftool
from datetime import datetime, timedelta

#set timer
start = time.time()

# users set the parameters (input, output folders, and gpx file
CSVFolder = r"C:\Users\Tommy Thompson\Documents\PythonClassF2016_FinalProject\Rec_31_Data_Test\Rec_31_CSV_Data_Test"
OutputFolder = r"C:\Users\Tommy Thompson\Documents\PythonClassF2016_FinalProject\Rec_31_Data_Test\Rec_31_CSV_Data_Test\Rec_31_Tif_OutputFolder_Test"
gpx_file = "C:\Users\Tommy Thompson\Documents\PythonClassF2016_FinalProject\Rec_31_Data_Test\GPX_Data\T20160226.gpx"
# user input? set year adjustment time for fliles date created modification
Year = 2016
TimeAdjus = 7

# open gpx
#read in point shapefile
gpx_ds = ogr.Open(gpx_file)
#get the first track_pointslayer
pntlyr = gpx_ds.GetLayer("track_points")
#print pntlyr.GetFeatureCount()
   
#register gdal & drivers
gdal.AllRegister()
driver = gdal.GetDriverByName('MEM')
driver.Register()

#get the exif tool to modify Tiff metadata file and start the tool
et = exiftool.ExifTool('exiftool.exe')
et.start()

#create loop that sorts through csv files and creats a tif image file for each csv found
# change directory to the csvfolder
os.chdir(CSVFolder)
#create dictionary to hold the csv timestamps
filenames={}   
# read through the csv file and do the work to create the tiff image
for fname in glob.glob("*.csv"):
    # Create data arrays for each csv file
    # Open each csv files and begin reading in the data rows
    # Skip first 5 rows for header data for each csv and start reading at row 6
    file = open(fname)
    HeaderRows=file.readlines()[0:5]
    # Get date and time from the 3rd row in the csv    
    DateTime = HeaderRows[2].strip().strip("Time = ").split(":")
    Date = datetime(Year,1,1,0,0,0) + timedelta(days=int(DateTime[0])-1,hours=int(DateTime[1]),minutes=int(DateTime[2]),seconds=round(float(DateTime[3])))
    #print Date        
    #creates date and time string
    TheDateString2 = Date.strftime('%a %b %d %H:%M:%S %Y')
    # Create a modified time to Set date modified on exported tiff file
    mtime = time.mktime(time.strptime(TheDateString2))
    # adjust the time if the use imputs a value
    MST = (mtime - (TimeAdjus*3600)) 
    # puts the mtime into the dictionary using fname as the iterator (modified time)
    ##filenames[mtime] = fname    
    # puts the MST into the dictionary using fname as the iterator (Mountain Standard Time)
    filenames[MST] = fname
    
# read through the gpx data
for row in pntlyr:
    # get elevation values as string
    elev = str(row.ele)
    # get latttitude, longitude values as string
    pnts = row.geometry()
    x = str(pnts.GetX())
    y = str(pnts.GetY())
    ##print pnts, x, y, elev
    
    # Create timestamp for gpx    
    gpxtime = time.mktime(time.strptime(row.time,'%Y/%m/%d %H:%M:%S+00'))
    # Modify gpx time    
    MST_gpxtime = (gpxtime - (TimeAdjus*3600))
    # If statement is true print matching times and then do the work on those matches
    if MST_gpxtime in filenames:
        print ("mst_gpxtime and gpx",filenames[MST_gpxtime], MST_gpxtime)
    ##if gpxtime in filenames:
        ##print ("modtime and gpx",filenames[gpxtime], gpxtime)
    
        # create and print data array
        DataArray = numpy.loadtxt(filenames[MST_gpxtime], dtype=float, delimiter=",", skiprows=5)
        DataArray = DataArray * 100
        DataArray.astype(int)
        ##print( DataArray)
    
        # rename each csv as tif
        output_imagename = os.path.join(OutputFolder, os.path.basename(filenames[MST_gpxtime]).replace('.csv', '.tif'))
        print (output_imagename)
    
        # get the pixel counts for x and y
        output_sizeX = 640
        output_sizeY = 512
    
        # create an output for each csv as tif
        FLIR_OutFile = driver.Create(output_imagename, output_sizeX, output_sizeY, 1, gdal.GDT_UInt16)
        inBand = FLIR_OutFile.GetRasterBand(1)
        inBand.WriteArray(DataArray)
        inBand.FlushCache()
            
        # creates GTIFF
        gdal.GetDriverByName('GTIFF').CreateCopy(output_imagename, FLIR_OutFile) 
        #Add GPS data (Long, Lat, Alt) to the metadata file for each of the images created.
        et.execute('-gps:GPSAltitude=' + elev , output_imagename) # where codes come from http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/GPS.html
        et.execute('-gps:GPSLatitude=' + y, output_imagename)
        et.execute('-gps:GPSLongitude=' + x, output_imagename)
        #set the new_gt on the FLIR_OutFile
        ##FLIR_OutFile.SetGeoTransform(new_gt)
        
        # create an if statment saying if the user enters a time adjusment value then use
        # that value to populate the tiffs time stamp with user time adjument value
        # else: the user does not enter a time adjustment then use mtime (modified time) to keep in GMT   
        if TimeAdjus !=0:    
            os.utime(output_imagename, (MST, MST))
        else:
            os.utime(output_imagename, (mtime, mtime)) 
        # remove FLIR_OutFile from memory
        del FLIR_OutFile
        # when you're done, call this to shut things down:
et.terminate()


#time it took to run the script
end= time.time()
elapesedtime= ((end-start)/60)
print ('It took ' , round(elapesedtime,2) , 'minutes to run this script')

