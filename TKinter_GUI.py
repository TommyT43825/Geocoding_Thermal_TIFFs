#  Date 12/05/2016
#  Author: Tommy Thompson
#  Ecology Graduate Student @ Utah Stat University
#  This GUI is for using a themal csv data sets (with the resolution of 640X512)
#  as input to create a tiff  and geotag each tiff image using gps data
#  the geotaged image time stamps must match timestamps from gps data
#****************************************************************************************************************

import logging
import gdal, glob, numpy, time, os, ogr
from datetime import datetime, timedelta
from Tkinter import *
import tkFileDialog, tkMessageBox
import exiftool


# log messager 
logging.basicConfig(filename=os.path.join(os.path.dirname(__file__), 'Geotag Thermal Images.log'),
                    format='%(asctime)s %(levelname)s: %(message)s',
                    datefmt='%m-%d %H:%M:%S',
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)

# creates class object for the GUI (create my own custom dialog name)
class GeoTag_Thermal_Images(Frame):
    """Class for the GUI."""

#-----------------------------------------------------------------------------------------------
# when dialog is called _init_ automatically runs
    def __init__(self, root, settings_fn):
        """Initialize the GUI.

        root:        master Tk object
        settings_fn: path to settings file
        """
# assigns the data types for the fields to be populated        
        Frame.__init__(self, root)
        self.in_fn = StringVar()
        self.fn_dir = StringVar()
        self.outfn_dir = StringVar()        
        self.year = IntVar()
        self.TimeAdjustment = IntVar()
        
# These lines of code creates the window size and titles it
        root.title('GeoTag Thermal Images')
        root.grid_columnconfigure(0, pad=10, weight=1)
        root.grid_rowconfigure(0, pad=20, weight=1)
        frame = Frame(root, padx=10, pady=10)
        frame.grid(row=0, column=0, sticky=W+E+N)
        self.status = Label(root, text='', padx=5, bd=1, relief=SUNKEN, anchor=W)
        self.status.grid(row=0, column=0, sticky=W+E+S)
        self.frame = frame
        frame.grid_columnconfigure(1, pad=10, weight=1)
        frame.grid_rowconfigure(1, pad=10)
        frame.grid_rowconfigure(3, pad=10)
        
# These are the labels for the fields of interest
        Label(frame, text='Input GPX File:').grid(row=0, column=0, sticky=E)
        Label(frame, text='Input Folder:').grid(row=1, column=0, sticky=E)        
        Label(frame, text='Output folder:').grid(row=2, column=0, sticky=E)
        Label(frame, text='Year:').grid(row=3, column=0, sticky=E)
        Label(frame, text='Time Adjustment hrs:').grid(row=4, column=0, sticky=E)        
        Label(frame, text='To adjust Time Zones use: (EST=5) (CST=6) (MST = 7) (PST=8)').grid(row=5, column=1, sticky=E) 
        
# create textbox      
        Entry(frame, textvariable=self.in_fn, width=40, state=DISABLED) \
            .grid(row=0, column=1, sticky=W+E, padx=10)
# create the input directory entry box
        self.input_entry = Entry(frame, textvariable=self.fn_dir, width=40, state=DISABLED)
        self.input_entry.grid(row=1, column=1, sticky=W+E, padx=10) 
# create the output directory entry box
        self.output_entry = Entry(frame, textvariable=self.outfn_dir, width=40, state=DISABLED)
        self.output_entry.grid(row=2, column=1, sticky=W+E, padx=10)        
# create year and time adjustment text boxes
        Entry(frame, textvariable=self.year, width=40).grid(row=3, column=1, sticky=W+E, padx=10)
        Entry(frame, textvariable=self.TimeAdjustment, width=30).grid(row=4, column=1, sticky=W+E, padx=10)
        
# creates buttons for input file, input folder and output folder
        Button(frame, text='Input file', command=self._get_in_fn).grid(row=0, column=2)
        Button(frame, text='Input folder', command=self._get_fn_dir).grid(row=1, column=2)
        Button(frame, text='Output folder', command=self._get_outfn_dir).grid(row=2, column=2)

# this is the button that runs the script        
        self.go = Button(frame, text='Bang Bang!', command=self._make_tiff)
        self.go.grid(row=4, column=1, columnspan=2, sticky=E)

#-----------------------------------------------------------------------------------------------
# create input file definition, open the gpx file dialog window
    def _get_in_fn(self):
        """Return an input .gpx name."""
        fn = tkFileDialog.askopenfilename(filetypes=[('GPX', '.gpx')],
                                        title='Select a gpx file',
                                        initialfile=self.in_fn.get())
# read input data (gpx data)
        if fn:
            self.in_fn.set(fn)
#-----------------------------------------------------------------------------------------------
# create input folder name definition            
    def _get_fn_dir(self):
            """Return an input folder directory."""
            in_dir = self.fn_dir.get()
            if not in_dir and self.in_fn.get():
                in_dir = os.path.basename(self.in_fn.get())
            # this reads in csv directory
            in_dir = tkFileDialog.askdirectory(initialdir=in_dir,
                                         mustexist=True,
                                         title='Select an input folder')
# read input directory
            if in_dir:
                self.fn_dir.set(in_dir)
#-----------------------------------------------------------------------------------------------
# create output folder name definition
    def _get_outfn_dir(self):
            """Return an output folder name."""
            out_fn = self.outfn_dir.get()
            if not out_fn and self.in_fn.get():
                out_fn = os.path.basename(self.in_fn.get())
            # this reads in and output directory for the tiff files to placed in
            out_fn = tkFileDialog.askdirectory(initialdir=out_fn,
                                         mustexist=True,
                                         title='Select an output folder')
# read output directory
            if out_fn:
                self.outfn_dir.set(out_fn)
                self._check_existing_files()            
#-----------------------------------------------------------------------------------------------                
# This code does most of the work for creating the geotagged thermal tiff images
# once the user hits the run button this definition runs the image processing code
    def _make_tiff(self):
        """Start the process to create the tiff from the csv."""
        # create the time adjustment variable from user input         
        TimeAdjus = self.TimeAdjustment.get()
        
        # open gpx
        gpx_file = self.in_fn.get()        
        gpx_ds = ogr.Open(gpx_file)
        #get the first track_pointslayer
        pntlyr = gpx_ds.GetLayer("track_points")
        ##print pntlyr.GetFeatureCount()
           
        # register gdal & drivers
        gdal.AllRegister()
        driver = gdal.GetDriverByName('MEM')
        driver.Register()
        
        # get the exif tool to modify Tiff metadata file and start the tool
        et = exiftool.ExifTool('exiftool.exe')
        et.start()
        
        # change directory to the csvfolder
        os.chdir(self.fn_dir.get())
        
        #create dictionary to hold the csv timestamps 
        filenames={}   
        
        # create loop that sorts through csv files and creats a tif image files for 
        # each csv that has a mathing gps timestamp        
        # read through the tiff images and add gps data to the metatdata
        for fname in glob.glob("*.csv"):
            # create data arrays for each csv file
            # open each csv files and begin reading in the data rows
            # skip first 5 rows for header data for each csv and start reading at row 6
            file = open(fname)
            HeaderRows=file.readlines()[0:5]
            # get date and time from the 3rd row in the csv    
            DateTime = HeaderRows[2].strip().strip("Time = ").split(":")
            Date = datetime(self.year.get(),1,1,0,0,0) + timedelta(days=int(DateTime[0])-1,
                            hours=int(DateTime[1]),minutes=int(DateTime[2]),seconds=round(float(DateTime[3])))
            ##print Date        
            
            # creates date and time string
            TheDateString2 = Date.strftime('%a %b %d %H:%M:%S %Y')
            # create a modified time to Set date modified on exported tiff file
            mtime = time.mktime(time.strptime(TheDateString2))
            # adjust the time with the user input value
            MST = (mtime - (TimeAdjus*3600)) 
            # puts the time adjustment into the dictionary using fname as the iterator 
            filenames[MST] = fname
            # not really Mountain Standard Time in all cases MST is just the varible name for time adjustment
            
        # read through the gpx data to get x, y, elevation, and timestamp data
        for row in pntlyr:
            # get elevation values as string
            elev = str(row.ele)
            # get latttitude, longitude values as string
            pnts = row.geometry()
            x = str(pnts.GetX())
            y = str(pnts.GetY())
            ##print pnts, x, y, elev
            
            # create timestamp for gpx    
            gpxtime = time.mktime(time.strptime(row.time,'%Y/%m/%d %H:%M:%S+00'))
            # modify gpx time to compare with csv time stamp data    
            MST_gpxtime = (gpxtime - (TimeAdjus*3600))
            # If statement is true print matching times and then do the work on those matches timestamps
            if MST_gpxtime in filenames:
                ##print ("mst_gpxtime and gpx",filenames[MST_gpxtime], MST_gpxtime)
            
                # create data array and multiply by 100 to make the file size smaller 
                DataArray = numpy.loadtxt(filenames[MST_gpxtime], dtype=float, delimiter=",", skiprows=5)
                DataArray = DataArray * 100
                DataArray.astype(int)
                ##print( DataArray)
            
                # rename each csv as tif using the matching csv and gpx time stamps
                output_imagename = os.path.join(self.outfn_dir.get(), os.path.basename(filenames[MST_gpxtime]).replace('.csv', '.tif'))
                print (output_imagename)
            
                # get the pixel counts for x and y
                output_sizeX = 640
                output_sizeY = 512
            
                # create an output for each csv as tif with 1 band and flush cache to remove from memory
                FLIR_OutFile = driver.Create(output_imagename, output_sizeX, output_sizeY, 1, gdal.GDT_UInt16)
                inBand = FLIR_OutFile.GetRasterBand(1)
                inBand.WriteArray(DataArray)
                inBand.FlushCache()
                    
                # creates GTIFF using gdal driver and use the exif tool to populate tiff metadata files
                gdal.GetDriverByName('GTIFF').CreateCopy(output_imagename, FLIR_OutFile) 
                # add GPS data (Long, Lat, Altitude) to the metadata file for each of the images created.
                et.execute('-gps:GPSAltitude=' + elev , output_imagename) # where codes come from http://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/GPS.html
                et.execute('-gps:GPSLatitude=' + y, output_imagename)
                et.execute('-gps:GPSLongitude=' + x, output_imagename)
            
                # create an if statment saying if the user enters a time adjusment value then use
                # that value to populate the tiffs time stamp with user time adjument value
                # else: the user does not enter a time adjustment then use mtime (modified time) to keep in GMT    
                if TimeAdjus !=0:    
                    os.utime(output_imagename, (MST, MST))
                else:
                    os.utime(output_imagename, (mtime, mtime)) 
                
                # remove FLIR_OutFile from memory
                del FLIR_OutFile
        # shut down the exif tool
        et.terminate()
        
#-----------------------------------------------------------------------------------------------           
# creates message box that asks user it they would like the existing files to be over written
    def _check_existing_files(self):
        """Make sure the user knows files will be overwritten."""
        dn = self.outfn_dir.get()
        if glob.glob(os.path.join(dn, '*.tif')) or glob.glob(os.path.join(dn, '*.tif')):
            msg = 'TIFF and TIFF files in {} will be overwritten if they have '.format(dn) + \
                  'the same name as created files. Use this folder anyway?'.format(dn)
            if not tkMessageBox.askyesno('Overwrite', msg):
                self.outfn_dir.set('')
#-----------------------------------------------------------------------------------------------
                
# Main code
if __name__=='__main__':
    #set timer
    start = time.time()
    settings_fn = os.path.join(os.path.dirname(__file__))
    root = Tk()
    GeoTag_Thermal_Images(root, settings_fn).grid()
    root.mainloop()
    # measure the time it took to run the script
end= time.time()
elapesedtime= ((end-start)/60)
print ('It took ' , round(elapesedtime,2) , 'minutes to run this script')